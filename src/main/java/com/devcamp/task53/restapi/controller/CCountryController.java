package com.devcamp.task53.restapi.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task53.restapi.Service.CCountryService;
import com.devcamp.task53.restapi.model.CCountry;
import com.devcamp.task53.restapi.model.CRegion;

@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
@RestController
public class CCountryController {
    @Autowired
    static CCountryService countries;

    @GetMapping("/countries")
    public List<CCountry> getCountryList() throws Exception {
        List<CCountry> allCountries = CCountryService.getCountryList();
        return allCountries;
    }

    @GetMapping("/country")
    public Map<String, Object> getRegionOfCountry(@RequestParam(required = true, name = "countrycode") int id)
            throws Exception {
        Map<String, Object> returnObject = new HashMap<String, Object>();
        List<CRegion> regionList = null;
        int i = 0;
        boolean isFounded = false;
        while (isFounded != true && i < CCountryService.getCountryList().size()) {
            if (CCountryService.getCountryList().get(i).getCountryCode() == id) {
                regionList = CCountryService.getCountryList().get(i).getRegions();
                isFounded = true;
                returnObject.put("region", regionList);
                returnObject.put("status", new String("OK!"));
            } else {
                i++;
                returnObject.put("region", null);
                returnObject.put("status", new String("Not found"));

            }
        }
        return returnObject;
    }

    @PostMapping("/countries/addnew")
    public CCountry createNewCountry(@RequestBody CCountry newCountry) {
        System.out.println(CCountryService.getCountryList().add(newCountry));

        CCountryService.getCountryList().add(newCountry);
        return newCountry;
    }
}
