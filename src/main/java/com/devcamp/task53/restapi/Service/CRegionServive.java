package com.devcamp.task53.restapi.Service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.devcamp.task53.restapi.model.CRegion;

@Service
public class CRegionServive {
    private static List<CRegion> regionVietnam = new ArrayList<CRegion>();
    private static List<CRegion> regionUsa = new ArrayList<CRegion>();
    private static List<CRegion> regionRussia = new ArrayList<CRegion>();

    static {
        CRegion dongnai = new CRegion("Tinh Dong Nai", 12);
        CRegion tphcm = new CRegion("Thanh Pho Ho Chi Minh", 13);
        CRegion hanoi = new CRegion("Thu Do Ha Noi", 29);
        CRegion Texas = new CRegion("Texas", 29);
        CRegion florida = new CRegion("Florida", 29);
        CRegion newyork = new CRegion("Newyork", 29);
        CRegion moscow = new CRegion("Moscow", 29);
        CRegion kaluga = new CRegion("Kaluga", 29);
        CRegion saintpeter = new CRegion("Saintpeter", 29);

        regionVietnam.add(dongnai);
        regionVietnam.add(tphcm);
        regionVietnam.add(hanoi);
        regionUsa.add(Texas);
        regionUsa.add(florida);
        regionUsa.add(newyork);
        regionRussia.add(moscow);
        regionRussia.add(kaluga);
        regionRussia.add(saintpeter);

    }

    public static List<CRegion> getRegionVietnam() {
        return regionVietnam;
    }

    public static void setRegionVietnam(List<CRegion> regionVietnam) {
        CRegionServive.regionVietnam = regionVietnam;
    }

    public static List<CRegion> getRegionUsa() {
        return regionUsa;
    }

    public static void setRegionUsa(List<CRegion> regionUsa) {
        CRegionServive.regionUsa = regionUsa;
    }

    public static List<CRegion> getRegionRussia() {
        return regionRussia;
    }

    public static void setRegionRussia(List<CRegion> regionRussia) {
        CRegionServive.regionRussia = regionRussia;
    }

}
