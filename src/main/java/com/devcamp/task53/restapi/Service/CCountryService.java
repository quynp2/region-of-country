package com.devcamp.task53.restapi.Service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.task53.restapi.model.CCountry;

@Service
public class CCountryService {
    private static List<CCountry> countryList = new ArrayList<CCountry>();

    @Autowired
    static CRegionServive regions;

    static {
        CCountry vietnam = new CCountry("Viet Nam", 1212, null);
        CCountry usa = new CCountry("Hoa Ky", 2022, null);
        CCountry russia = new CCountry("Nga", 2023, null);
        countryList.add(vietnam);
        countryList.add(usa);
        countryList.add(russia);

        for (int i = 0; i < countryList.size(); i++) {
            if (countryList.get(i).getCountryName() == "Viet Nam") {
                countryList.get(i).setRegions(CRegionServive.getRegionVietnam());
            } else if (countryList.get(i).getCountryName() == "Hoa Ky") {
                countryList.get(i).setRegions(CRegionServive.getRegionUsa());
            } else if (countryList.get(i).getCountryName() == "Nga") {
                countryList.get(i).setRegions(CRegionServive.getRegionRussia());
            }
        }

    }

    public CCountryService() {
        super();
    }

    public static List<CCountry> getCountryList() {
        return countryList;
    }

    public static void setCountryList(List<CCountry> countryList) {
        CCountryService.countryList = countryList;
    }

}
